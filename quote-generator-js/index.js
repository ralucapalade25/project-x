var arrayOfQuotes = [
{
    "author":'Oscar Wilde',
    "quote":'To live is the rarest thing in the world. Most people exist, that is all.'
},

{
    "author":'Emily Dickinson',
    "quote":'That it will never come again is what makes life so sweet.'
},

{
    "author":'George Eliot',
    "quote":'It is never too late to be what you might have been.'
},

{
    "author":"William Shakespeare",
    "quote":"All the world's a stage, and all the men and women merely players."
},

{
    "author":"Ernest Hemingway",
    "quote":"We are all broken, that's how the light gets in."
},

{
    "author":'Stephen King',
    "quote":'Amateurs sit and wait for inspiration, the rest of us just get up and go to work.' 
}


]

function randomSelector(arrayLength){
    return Math.floor(Math.random()*arrayLength);
}

function generateQuote(){
    var randomNumber = randomSelector(arrayOfQuotes.length);
    document.getElementById('quoteOutput').innerHTML = '"' + arrayOfQuotes[randomNumber].quote + '"';
    document.getElementById('authorOutput').innerHTML = '-' + arrayOfQuotes[randomNumber].author;


}