
var number1 = Math.floor((Math.random()*10)+1);
var number2 = Math.floor((Math.random()*10)+1);

document.getElementById('number1').innerHTML = number1;
document.getElementById('number2').innerHTML = number2;

var checkAnswer = document.querySelector('input[type=text]');
var value = checkAnswer.value;
var btn = document.querySelector('input[type=button][value=check]');

btn.onclick = function(){
    var answer = number1 + number2;
    value = checkAnswer.value;
    if(value == answer){
        alert('The answer is correct');
    }else{
        alert('The answer is incorrect, the answer was ' + answer);
    }

    document.querySelector('input[type=text]').value = "";
    number1 = Math.floor((Math.random()*10)+1);
    number2 = Math.floor((Math.random()*10)+1);
    document.getElementById('number1').innerHTML = number1;
    document.getElementById('number2').innerHTML = number2;

}