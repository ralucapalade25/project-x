const firstElement = document.querySelector('#globalScale1');
const secondElement = document.querySelector('#globalScale2');
const thirdElement = document.querySelector('#globalScale3');
const fourthElement = document.querySelector('#globalScale4');
const fifthElement = document.querySelector('#communicationPercentage');
const sixthElement = document.querySelector('#availability');
const seventhElement = document.querySelector('#teamSpeed');


axios.get('http://localhost:4951/global-scale')
        .then(response => {
            console.log(response);
            firstElement.innerHTML = response.data.globalScale1
            secondElement.innerHTML = response.data.globalScale2 + "%"
            thirdElement.innerHTML = response.data.globalScale3 + "+"
            fourthElement.innerHTML = response.data.globalScale4 + "+"
            fifthElement.innerHTML = response.data.communicationPercentage + "%"
            sixthElement.innerHTML = response.data.availability
            seventhElement.innerHTML = response.data.teamSpeed
          })
          .catch(error => {
            console.log(error);
          });
    
    
    
    