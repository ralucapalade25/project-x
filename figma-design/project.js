setInterval(openForm, 300000)

function openForm(){
    document.getElementById("open-form").style.display = "block";
}

function closeForm() {
    document.getElementById("open-form").style.display = "none";
  }

const form = document.getElementById("myForm");
form.addEventListener('submit', function(e){
    e.preventDefault();
    const formData = new FormData(form);
    
    const data = {name: formData.get('fullname'),
                email: formData.get('email'),
                phonenumber: formData.get('phonenumber')}

    axios.post('http://localhost:4949/subscribe', data)
        .then(res => console.log(res))
        
        .catch((err) => {
                console.log(err);
                console.log(err.response.data);
                document.getElementById("message").innerHTML = err.response.data             
           }); 

})

    
 
